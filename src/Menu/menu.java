package Menu;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class menu {

    private static int nerd;
    private static double porcentaje;
    private static FormNerd f1;

    public static void main(String[] args) {
        int op = 0;
        LocalDate fechaNac;
        //PaolaCussi
        CalculoImc imc = new CalculoImc();
        //Variables utilizadas en el case 6 (Duran Miguel
        Preguntas p = new Preguntas();
        int puntaje;
        //Vsriables 7 Dante Lamas
        cuadrante C = new cuadrante();

        do {
            try {
                op = Integer.parseInt(JOptionPane.showInputDialog(null,
                        "1. Saludar (Martinez Jose)"
                        + "\n2. Calcular su edad (Chazarreta Horacio)"
                        + "\n3. Conocer signo del Zodiaco (Cordoba Daniel)"
                        + "\n4. realizar una suma (Ferreira Marcos)"
                        + "\n5. Calcular IMC (Cussi Paola)"
                        + "\n6. Conociendo mi YO interior (Duran Miguel)"
                        + "\n7. Cuadrantes (Lamas Dante)"
                        + "\n8. Ordenar Numeros (Palacio A)"
                        + "\n9. Serie de Fibonacci! (Cabrera Josefina)"
                        + "\n10. Factorial de un Número (Ariel Burgos)"
                        + "\n99. Salir", "Menú", 3));
                switch (op) {
                    case 1://(Martinez Jose)
                        JOptionPane.showMessageDialog(null, "Hola");
                        break;
                    case 2://(Chazarreta Horacio)
                        try {
                            fechaNac = LocalDate.parse(JOptionPane.showInputDialog("Ingrese su fecha de nacimiento en formato (dd/MM/yyyy)"),
                                    DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                            // Se obtiene la fecha actual yyyy-MM-dd
                            LocalDate fechaActual = LocalDate.now();
                            // Calculo diferencia entre las fechas
                            Period diff = Period.between(fechaNac, fechaActual);
                            JOptionPane.showMessageDialog(null, ("Tiene " + diff.getYears() + " años "
                                    + diff.getMonths() + " meses " + diff.getDays() + " días"));
                        } catch (DateTimeParseException dte) {
                            JOptionPane.showMessageDialog(null, "¡Fecha Incorrecta!");
                        } catch (NullPointerException npe) {
                            JOptionPane.showMessageDialog(null, "Ingreso cancelado");
                        }
                        break;
                    case 3:// (Cordoba Daniel)
                        signoZodiaco();
                        break;
                    case 4:// Ferreira Marcos
                        int a,
                         b,
                         suma;
                        a = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresar un numero ", "numero 1", 3));
                        b = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresar otro numero ", "numero 2", 3));
                        suma = a + b;
                        JOptionPane.showMessageDialog(null, "la suma es:  " + suma);
                        break;
                    //PaolaCussi   
                    case 5:
                        imc.Calcular();
                        break;
                    case 6: //Duran Miguel
                        JOptionPane.showMessageDialog(null, "este test es para averiguar que tan NERD eres", "TEST", 1);
                        puntaje = p.pregunta1();
                        puntaje += p.pregunta2();
                        puntaje += p.pregunta3();
                        puntaje += p.pregunta4();
                        puntaje += p.pregunta5();
                        puntaje += p.pregunta6();
                        puntaje += p.pregunta7();
                        puntaje += p.pregunta8();
                        puntaje += p.pregunta9();
                        puntaje += p.pregunta10();
                        porcentaje = puntaje * 100 / 110;
                        calcularNerd(porcentaje);
                        mostrarGradoNerd();
                        break;
                    case 7: //Lamas Dante
                        C.Cuadrante();
                        break;
                    case 8:
                        ordenarNumeros();
                        break;
                    case 9:

                        // Cabrera Josefina  
                        Scanner teclado = new Scanner(System.in);
                        int aa = 0,
                         bb = 1,
                         cc,
                         n;
                        System.out.println("\nSerie de Fibonacci");
                        System.out.print("Por favor ingrese cantidad de elementos para la serie: ");
                        n = teclado.nextInt();
                        System.out.println("La serie es: ");
                        for (int i = 0; i < n; i++) {
                            System.out.println(aa);
                            cc = aa + bb;
                            aa = bb;
                            bb = cc;
                        }
                        break;
                    case 10://Ariel Burgos
                        int x = 0;
                        x = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresar un numero ", "numero 1", 3));
                        int aux = x;
                        int resultado = 1;
                        while (aux != 0) {
                            resultado = resultado * aux;
                            aux--;
                        }
                        JOptionPane.showMessageDialog(null, "El factorial del numero " + x + " es " + resultado, "Factorial", 1);

                        break;
                    case 99:
                        JOptionPane.showMessageDialog(null, "Fin del Programa");
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "¡¡¡Opcion No Encontrada!!!");
                }
            } catch (NumberFormatException x) {
                JOptionPane.showMessageDialog(null, "Ingrese un numero!!!", "Error", 3);
            }
        } while (op != 99);

    }

    //CASE 8 (PALACIOA)+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public static void ordenarNumeros() {
        double mayor, medio, menor, n1, n2, n3;

        n1 = Double.parseDouble(JOptionPane.showInputDialog("Ingrese 1° número"));
        n2 = Double.parseDouble(JOptionPane.showInputDialog("Ingrese 2° número"));
        n3 = Double.parseDouble(JOptionPane.showInputDialog("Ingrese 2° número"));

        mayor = n1;
        medio = n1;
        menor = n1;

        if (n1 > mayor) {
            mayor = n1;
        }
        if (n2 > mayor) {
            mayor = n2;
        }
        if (n3 > mayor) {
            mayor = n3;
        }
        if (n1 < menor) {
            menor = n1;
        }
        if (n2 < menor) {
            menor = n2;
        }
        if (n3 < menor) {
            menor = n3;
        }
        if (n1 < mayor && n1 > menor) {
            medio = n1;
        }
        if (n2 < mayor && n2 > menor) {
            medio = n2;
        }
        if (n3 < mayor && n3 > menor) {
            medio = n3;
        }

        JOptionPane.showMessageDialog(null, "De Mayor a Menor | " + mayor + "  |  " + medio + "  |  " + menor + " |");
        JOptionPane.showMessageDialog(null, "De Menor a Mayor | " + menor + "  |  " + medio + "  |  " + mayor + " |");
    }

    //case 6 (Duran Miguel)
    private static void calcularNerd(double porcentaje) {
        if (porcentaje > 85) {
            nerd = 1;
        } else if (porcentaje > 65) {
            nerd = 2;
        } else if (porcentaje > 50) {
            nerd = 3;
        } else if (porcentaje > 35) {
            nerd = 4;
        } else {
            nerd = 5;
        }

    }

    private static void mostrarGradoNerd() {
        f1 = new FormNerd();
        switch (nerd) {
            case 1:
                FormNerd.txtNerd.setText("RESULTADO: INCREIBLE ERES " + porcentaje + "% NERD");
                FormNerd.txtDescripcion.setText("Eres súper nerd. ¡El nerdómetro está a punto de explotar contigo \n"
                        + "Desde chiquito/a fuiste una persona a la que le interesaba \n"
                        + "aprender y aun a tu edad no hay nada que te emocione más que el conocimiento.\n"
                        + "Tu curiosidad es imparable; siempre quieres saber de qué están \n"
                        + "hechas las cosas y aprender a hacerlas con tus propias manos. \n"
                        + "Pocas personas entienden tu mundo, pero las que sí lo hacen,\n"
                        + "son tus amistades más preciadas.");
                f1.setVisible(true);
                break;
            case 2:
                FormNerd.txtNerd.setText("RESULTADO: ASOMBROSO ERES " + porcentaje + "% NERD");
                FormNerd.txtDescripcion.setText("Tu porcentaje de nerd es alto, peeeero no llegas al máximo puntaje.\n"
                        + "Esto es porque, por un lado, te gusta la ciencia ficción, las matemáticas y \n"
                        + "los programas de tele científicos. Pero, por otro lado, no te la vives estudiando o \n"
                        + "pegado/a a la compu. Eres una persona a la que le interesa mucho pasar tiempo \n"
                        + "dentro de su mente, pero que también valora salir a echar desmadre con los amigos, \n"
                        + "reírse por cosas tontas y ver películas ligeras.");
                f1.setVisible(true);
                break;
            case 3:
                FormNerd.txtNerd.setText("RESULTADO: ASOMBROSO ERES " + porcentaje + "% NERD");
                FormNerd.txtDescripcion.setText("En tu interior hay una lucha terrible entre dos lobos: \n"
                        + "Uno es malvado y está lleno de ira,\n"
                        + "el otro es un tetazo y escribe fan fiction de Dr. Who.\n"
                        + "Ganará aquel al que tú alimentes. La posta, el lobo nerd es más chido.");
                f1.setVisible(true);
                break;
            case 4:
                FormNerd.txtNerd.setText("RESULTADO: VAYA!! ERES " + porcentaje + "% NERD");
                FormNerd.txtDescripcion.setText("Todo parece indicar que eres un nerdicurioso. \n"
                        + "No te identificas como un nerd pero compartes algunos de sus intereses. \n"
                        + "Que no te importe lo que diga la sociedad. Tú vive tu nerdés libre de prejuicios.");
                f1.setVisible(true);
                break;
            case 5:
                FormNerd.txtNerd.setText("RESULTADO: NO ME SORPRENDE ERES " + porcentaje + "% NERD");
                FormNerd.txtDescripcion.setText("No eres nada nerd. Es probable que no te gusten las matemáticas,\n"
                        + "ni los canales de ciencia, ni que te regalen libros. \n"
                        + "De hecho, hiciste este quiz solo para demostrar lo muy NO NERD que eres. \n"
                        + "Pero, ¿a poco no te dieron ganas de pasarte al lado nerd de la vida? \n"
                        + "¡Anímate, hay muchos cómics, juegos en línea y pelis de ciencia ficción!.");
                f1.setVisible(true);
                break;
        }
    }

    //Case 3 (Cordoba Daniel)
    public static void signoZodiaco() {
        int dia, mes;
        dia = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresar dia que naciste ", "Dia que naciste", 3));
        mes = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresar mes que naciste ", "Mes que naciste", 3));
        if (mes >= 1 && mes <= 12) {
            switch (mes) {
                case 1: //Enero
                    if (dia >= 1 && dia <= 31) {
                        if (dia >= 22) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Acuario"
                                    + "\n\nUn Acuario es simpatico y humanitario. Es honesto y totalmente leal, original"
                                    + "\ny brillante. Un Acuario es independiente e intelectual. Les gusta luchar por causas"
                                    + "\nbuenas, soñar y planificar para un futuro feliz, aprender del pasado, los buenos"
                                    + "\namigos y divertirse.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Capricornio"
                                    + "\n\nUn Capricornio tiene ambicion y es disciplinado. Es practico y prudente,"
                                    + "\ntiene paciencia y hasta es cauteloso cuando hace falta. Tiene un buen sentido"
                                    + "\nde humor y es reservado. A un capricornio le gusta la fiabilidad, el "
                                    + "\nprofecionalismo, una base solida, tener un objetivo, el liderazgo.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 2: //Febrero
                    if (dia >= 1 && dia <= 29) {
                        if (dia >= 19) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Piscis"
                                    + "\n\nUn Piscis es imaginativo y sensible, es amable y tiene compasion hacia"
                                    + "\nlos demas. Es intuitivo y piensa en los demas. Piscis tiene una personalidad"
                                    + "\ntranquila y tiene una gran capacidad creativa en el terreno de lo artistico.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Acuario"
                                    + "\n\nUn Acuario es simpatico y humanitario. Es honesto y totalmente leal, original"
                                    + "\ny brillante. Un Acuario es independiente e intelectual. Les gusta luchar por causas"
                                    + "\nbuenas, soñar y planificar para un futuro feliz, aprender del pasado, los buenos"
                                    + "\namigos y divertirse.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 3: //Marzo
                    if (dia >= 1 && dia <= 31) {
                        if (dia >= 21) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Aries"
                                    + "\n\nLos Aries son aventureros y energeticos, son pioneros y valientes."
                                    + "\nSon listos, dinamicos, seguros de si y suelen demostrar entusiasmo "
                                    + "\nhacia las cosas. Les gusta la aventura y los retos. Les gusta ganar"
                                    + "\ny son espontaneos. Tambien le gusta dar su apoyo a una buena causa.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Piscis"
                                    + "\n\nUn Piscis es imaginativo y sensible, es amable y tiene compasion hacia"
                                    + "\nlos demas. Es intuitivo y piensa en los demas. Piscis tiene una personalidad"
                                    + "\ntranquila y tiene una gran capacidad creativa en el terreno de lo artistico.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 4: //Abril
                    if (dia > 0 && dia <= 30) {
                        if (dia >= 22) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Tauro! "
                                    + "\n\nTauro es paciente, persistente, decidido y fiable. Le encanta"
                                    + "\nsentirse seguro. Tiene buen corazon y es muy cariñoso. A un Tauro"
                                    + "\nle gusta la estabilidad, las cosas naturales, el placer y la"
                                    + "\ncomodidad. Los tauro disfrutan con tiempo para reflexionar y les"
                                    + "\nencanta sentirse atraido hacia alguien.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Aries"
                                    + "\n\nLos Aries son aventureros y energeticos, son pioneros y valientes."
                                    + "\nSon listos, dinamicos, seguros de si y suelen demostrar entusiasmo "
                                    + "\nhacia las cosas. Les gusta la aventura y los retos. Les gusta ganar"
                                    + "\ny son espontaneos. Tambien le gusta dar su apoyo a una buena causa.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 5: //Mayo
                    if (dia > 0 && dia <= 31) {
                        if (dia >= 22) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Geminis"
                                    + "\n\nLos Geminis son intelectuales, elocuentes, cariñosos, comunicativos"
                                    + "\ne inteligentes. Tienen mucha energia y vitalidad. Les gusta hablar,"
                                    + "\nleer, hacer varias cosas a la vez. Los geminis disfrutan con lo inusual"
                                    + "\ny la novedad. cuanto mas variedad en su vida, mejor.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Tauro! "
                                    + "\n\nTauro es paciente, persistente, decidido y fiable. Le encanta"
                                    + "\nsentirse seguro. Tiene buen corazon y es muy cariñoso. A un Tauro"
                                    + "\nle gusta la estabilidad, las cosas naturales, el placer y la"
                                    + "\ncomodidad. Los tauro disfrutan con tiempo para reflexionar y les"
                                    + "\nencanta sentirse atraido hacia alguien.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 6: //Junio
                    if (dia > 0 && dia <= 30) {
                        if (dia >= 22) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Cancer"
                                    + "\n\nUn Cancer es emocional y cariñoso, protector y simpatico. Un cancer"
                                    + "\ntiene mucha imaginacion e intuicion y sabe ser cauteloso cuando hace "
                                    + "\nfalta. A un cancer le gusta su casa, el campo, los niños, disfrutar"
                                    + "\ncon sus aficiones y le gustan las fiestas.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Geminis"
                                    + "\n\nLos Geminis son intelectuales, elocuentes, cariñosos, comunicativos"
                                    + "\ne inteligentes. Tienen mucha energia y vitalidad. Les gusta hablar,"
                                    + "\nleer, hacer varias cosas a la vez. Los geminis disfrutan con lo inusual"
                                    + "\ny la novedad. cuanto mas variedad en su vida, mejor.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 7: //Julio
                    if (dia > 0 && dia <= 31) {
                        if (dia >= 22) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Leo"
                                    + "\n\nUn Leo es generoso y bondadoso, fiel y cariñoso. Un leo es creativo,"
                                    + "\nentusiasta y comprensivo con los demas. Le gusta la aventura, el lujo "
                                    + "\ny la comodidad. Un leo disfruta con los niños, el teatro y las fiestas."
                                    + "\nTambien le motiva el riesgo.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Cancer"
                                    + "\n\nUn Cancer es emocional y cariñoso, protector y simpatico. Un cancer"
                                    + "\ntiene mucha imaginacion e intuicion y sabe ser cauteloso cuando hace "
                                    + "\nfalta. A un cancer le gusta su casa, el campo, los niños, disfrutar"
                                    + "\ncon sus aficiones y le gustan las fiestas.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 8: //Agosto
                    if (dia > 0 && dia <= 31) {
                        if (dia >= 23) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Virgo"
                                    + "\n\nLos Virgos suelen ser meticulosos, practicos y trabajadores. Tienen"
                                    + "\ngran capacidad analistica y son fiables. A los virgos les gusta la vida"
                                    + "\nsana, hacer listas, el orden y la higiene.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Leo"
                                    + "\n\nUn Leo es generoso y bondadoso, fiel y cariñoso. Un leo es creativo,"
                                    + "\nentusiasta y comprensivo con los demas. Le gusta la aventura, el lujo "
                                    + "\ny la comodidad. Un leo disfruta con los niños, el teatro y las fiestas."
                                    + "\nTambien le motiva el riesgo.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 9: //Septiembre
                    if (dia > 0 && dia <= 30) {
                        if (dia >= 24) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Libra"
                                    + "\n\nUn Libra es diplomatico, encantador y sociable. Los libra son idealistas,"
                                    + "\npacificos, optimistas y romanticos. Tienen un caracter afable y equilibrado."
                                    + "\nLos libra se encuentran entre los signos mas civilizados del zodiaco.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Virgo"
                                    + "\n\nLos Virgos suelen ser meticulosos, practicos y trabajadores. Tienen"
                                    + "\ngran capacidad analistica y son fiables. A los virgos les gusta la vida"
                                    + "\nsana, hacer listas, el orden y la higiene.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 10: //Octubre
                    if (dia > 0 && dia <= 31) {
                        if (dia >= 22) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Escorpio"
                                    + "\n\nUn Escorpio es emocional, decidido, poderoso y apasionado. El Escorpio"
                                    + "\nes un signo con mucho magnetismo. A un Escorpio le gusta la verdad, el"
                                    + "\ntrabajo cuando tiene sentido, involucrarse en causas y convencer a los demas.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Libra"
                                    + "\n\nUn Libra es diplomatico, encantador y sociable. Los libra son idealistas,"
                                    + "\npacificos, optimistas y romanticos. Tienen un caracter afable y equilibrado."
                                    + "\nLos libra se encuentran entre los signos mas civilizados del zodiaco.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 11: //Noviembre
                    if (dia > 0 && dia <= 30) {
                        if (dia >= 23) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Sagitario"
                                    + "\n\nUn Sagitario es intelectual, honesto, sincero y simpatico. A los sagitario"
                                    + "\nles caracteriza el optimismo, su modestia y su buen humor. A los sagitario les"
                                    + "\ngusta la libertad, viajar, las leyes, la aventura. Es uno de los signos mas "
                                    + "\npositivos del zodiaco. Son versatiles, les encanta la aventura y lo desconocido.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Escorpio"
                                    + "\n\nUn Escorpio es emocional, decidido, poderoso y apasionado. El Escorpio"
                                    + "\nes un signo con mucho magnetismo. A un Escorpio le gusta la verdad, el"
                                    + "\ntrabajo cuando tiene sentido, involucrarse en causas y convencer a los demas.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 12: //Diciembre
                    if (dia > 0 && dia <= 31) {
                        if (dia >= 22) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Capricornio"
                                    + "\n\nUn Capricornio tiene ambicion y es disciplinado. Es practico y prudente,"
                                    + "\ntiene paciencia y hasta es cauteloso cuando hace falta. Tiene un buen sentido"
                                    + "\nde humor y es reservado. A un capricornio le gusta la fiabilidad, el "
                                    + "\nprofecionalismo, una base solida, tener un objetivo, el liderazgo.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Sagitario"
                                    + "\n\nUn Sagitario es intelectual, honesto, sincero y simpatico. A los sagitario"
                                    + "\nles caracteriza el optimismo, su modestia y su buen humor. A los sagitario les"
                                    + "\ngusta la libertad, viajar, las leyes, la aventura. Es uno de los signos mas "
                                    + "\npositivos del zodiaco. Son versatiles, les encanta la aventura y lo desconocido.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;

            }
        } else {
            JOptionPane.showMessageDialog(null, "El mes ingresado es incorrecto!", "Error", 3);
        }
    }
}
