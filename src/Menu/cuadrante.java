/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menu;

import javax.swing.JOptionPane;

/**
 *
 * @author DANTE LAMAS
 */
public class cuadrante {

    int x, y;

    public cuadrante() {
    }

    public void Cuadrante() {

        x = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un valor para el punto X (Distinto de 0)"));

        y = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un valor para el punto Y (Distinto de 0)"));

        if (x > 0 && y > 0) {
            JOptionPane.showMessageDialog(null,"El punto (" + x + "," + y + ") esta en el primer cuadrante");
        } else if (x < 0 && y > 0) {
           JOptionPane.showMessageDialog(null,"El punto (" + x + "," + y + ") esta en el segundo cuadrante");
        } else if (x < 0 && y < 0) {
        JOptionPane.showMessageDialog(null,"El punto (" + x + "," + y + ") esta en el tercer cuadrante");
        } else if (x > 0 && y < 0) {
           JOptionPane.showMessageDialog(null,"El punto (" + x + "," + y + ") esta en el cuarto cuadrante");
        }
    }
}
