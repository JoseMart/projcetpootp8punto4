/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menu;

import javax.swing.JOptionPane;

/**
 *
 * @author Mauro
 */
public class Preguntas {
    public int res=0;
    int op;
    boolean bandera;
    
    public  int pregunta1() {
        bandera=true;
        while (bandera){
            try{
            op = Integer.parseInt(JOptionPane.showInputDialog(null,
                        "Para empezar, cuentame qué tal iba en la secundaria: \n"
                        + "1) Súper bien, puro 10 o 9\n"
                        + "2) Casi siempre sacaba 8 o 7\n"
                        + "3) Pésimo, de 6 para abajo\n"
                        ,"PREGUNTA 1", 3));            
            }catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Opcion Incorrecta...Pruebe otra vez");
            }
            if (op>=1 && op<=3)
                bandera=false;
        }
        if(op==1){
            res=10;
        }else if(op==2){
            res=6;
        }else{
            res=3;
        }
        return res;
    }
    
    public  int pregunta2() {
        bandera=true;
        while (bandera){
            try{
            op = Integer.parseInt(JOptionPane.showInputDialog(null,
                        "¿Alguna vez te has puesto a leer Wikipedia sin buscar nada en específico,"
                        + " como si fuera un libro? \n"
                        + "1) Nunca\n"
                        + "2) Poquitas veces\n"
                        + "3) Todo el tiempo lo hago, me encanta\n"
                        ,"PREGUNTA 2", 3));            
            }catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Opcion Incorrecta...Pruebe otra vez");
            }
            if (op>=1 && op<=3)
                bandera=false;
        }
        if(op==1){
            res=2;
        }else if(op==2){
            res=6;
        }else{
            res=10;
        }
        return res;
    }
    
    public  int pregunta3() {
        bandera=true;
        while (bandera){
            try{
            op = Integer.parseInt(JOptionPane.showInputDialog(null,
                        "En la facu, ¿muchos compañeros te piden ayuda para entender ciertas materias? \n"
                        + "1) Poco, en una o pocas materias\n"
                        + "2) ¡Siempre, en muchas materias!\n"
                        + "3) Nunca\n"
                        ,"PREGUNTA 3", 3));            
            }catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Opcion Incorrecta...Pruebe otra vez");
            }
            if (op>=1 && op<=3)
                bandera=false;
        }
        if(op==1){
            res=5;
        }else if(op==2){
            res=10;
        }else{
            res=3;
        }
        return res;
    }
    
    public  int pregunta4() {
        bandera=true;
        while (bandera){
            try{
            op = Integer.parseInt(JOptionPane.showInputDialog(null,
                        "En la primaria, ¿en qué parte del aula te sentabas? \n"
                        + "1) En la primera fila\n"
                        + "2) En el medio\n"
                        + "3) En los ultimos asientos\n"
                        ,"PREGUNTA 4", 3));            
            }catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Opcion Incorrecta...Pruebe otra vez");
            }
            if (op>=1 && op<=3)
                bandera=false;
        }
        if(op==1){
            res=10;
        }else if(op==2){
            res=6;
        }else{
            res=3;
        }
        return res;
    }
    
    public  int pregunta5() {
        bandera=true;
        while (bandera){
            try{
            op = Integer.parseInt(JOptionPane.showInputDialog(null,
                        "¿Sabes cuánta memoria RAM tiene tu computadora? \n"
                        + "1) Por supuesto que no, ni siquiera sé qué es eso\n"
                        + "2) Sé qué es, pero no sé cuánta tiene mi compu\n"
                        + "3) Obvio lo sé\n"
                        + "4) Lo sé porque, de hecho, yo se la expandí\n"
                        ,"PREGUNTA 5", 3));            
            }catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Opcion Incorrecta...Pruebe otra vez");
            }
            if (op>=1 && op<=4)
                bandera=false;
        }
        switch (op) {
            case 1: res=1; break;
            case 2: res=4; break;
            case 3: res=8; break;
            case 4: res=10; break;
        }
        return res;
    }
    
    public  int pregunta6() {
        bandera=true;
        while (bandera){
            try{
            op = Integer.parseInt(JOptionPane.showInputDialog(null,
                        "¿Eres esa típica persona que desarma sus cosas para modificarlas y mejorarlas? \n"
                        + "1) No, para nada\n"
                        + "2) Ay, sí, me encanta hacerlo\n"                       
                        ,"PREGUNTA 6", 3));            
            }catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Opcion Incorrecta...Pruebe otra vez");
            }
            if (op>=1 && op<=2)
                bandera=false;
        }
        if(op==1){
            res=4;
        }else{ 
            res=10;
        }
        return res;
    }
    
    public  int pregunta7() {
        bandera=true;
        while (bandera){
            try{
            op = Integer.parseInt(JOptionPane.showInputDialog(null,
                        "¿En qué lenguajes sabes programar? \n"
                        + "1) HTML y CSS.\n"
                        + "2) PHP y Javascript.\n"
                        + "3) Python y Ruby.\n"
                        + "4) Todos los anteriores.\n"
                        + "5) Ugh, cómo se ve que esta lista no la hizo un programador.\n"
                        ,"PREGUNTA 7", 3));            
            }catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Opcion Incorrecta...Pruebe otra vez");
            }
            if (op>=1 && op<=5)
                bandera=false;
        }
        switch (op) {
            case 1: res=4; break;
            case 2: res=3; break;
            case 3: res=8; break;
            case 4: res=10; break;
            case 5: res=2; break;
        }
        return res;
    }
    
    public  int pregunta8() {
        bandera=true;
        while (bandera){
            try{
            op = Integer.parseInt(JOptionPane.showInputDialog(null,
                        "¿Has jugado videojuegos más de cinco horas seguidas? \n"
                        + "1) Nunca\n"
                        + "2) Si\n"
                        + "3) ¿Sólo cinco?\n"
                        ,"PREGUNTA 8", 3));            
            }catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Opcion Incorrecta...Pruebe otra vez");
            }
            if (op>=1 && op<=3)
                bandera=false;
        }
        if(op==1){
            res=2;
        }else if(op==2){
            res=7;
        }else{
            res=10;
        }
        return res;
    }
    
    public  int pregunta9() {
        bandera=true;
        while (bandera){
            try{
            op = Integer.parseInt(JOptionPane.showInputDialog(null,
                        "¿Has organizado una LAN party? \n"
                        + "1) Nunca\n"
                        + "2) Un par de veces\n"
                        + "3) Muy seguido\n"
                        ,"PREGUNTA 9", 3));            
            }catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Opcion Incorrecta...Pruebe otra vez");
            }
            if (op>=1 && op<=3)
                bandera=false;
        }
        if(op==1){
            res=1;
        }else if(op==2){
            res=6;
        }else{
            res=10;
        }
        return res;
    }
    
    public  int pregunta10() {
        bandera=true;
        while (bandera){
            try{
            op = Integer.parseInt(JOptionPane.showInputDialog(null,
                        "¿Viste la serie completa de Game of Thrones? \n"
                        + "1) No, que es eso...\n"
                        + "2) La vi, pero no me atrapo\n"
                        + "3) Obvio si. Dracarys\n"
                        ,"PREGUNTA 10", 3));            
            }catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Opcion Incorrecta...Pruebe otra vez");
            }
            if (op>=1 && op<=3)
                bandera=false;
        }
        if(op==1){
            res=1;
        }else if(op==2){
            res=6;
        }else{
            res=10;
        }
        return res;
    }
    
    public  int pregunta11() {
        bandera=true;
        while (bandera){
            try{
            op = Integer.parseInt(JOptionPane.showInputDialog(null,
                        "¿Eres más Marvel o DC? \n"
                        + "1) Marvel\n"
                        + "2) DC\n"
                        + "3) Todo menos esos cómics de Hollywood.\n"
                        + "4) Uy no, joven. Yo no le hago a los súper héroes.\n"
                        ,"PREGUNTA 11", 3));            
            }catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Opcion Incorrecta...Pruebe otra vez");
            }
            if (op>=1 && op<=4)
                bandera=false;
        }
        switch (op) {
            case 1: res=10; break;
            case 2: res=9; break;
            case 3: res=4; break;
            case 4: res=1; break;
        }
        return res;
    }
}
