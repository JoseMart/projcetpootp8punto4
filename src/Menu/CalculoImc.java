/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menu;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author Paola
 */
public class CalculoImc { 
    
         double peso, estatura,indice;
        Scanner dato = new Scanner(System.in);

    public CalculoImc() {
    }
        
        public void Calcular(){
        peso = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el peso"));        
        
        String est = JOptionPane.showInputDialog("Ingrese la estatura");
        
        estatura = Double.parseDouble(est);        
        
        if(peso>0 && estatura>0){
            indice= peso/(estatura*estatura);
            JOptionPane.showMessageDialog(null,"Su IMC es "+indice);
            if(indice<18.5){
                JOptionPane.showMessageDialog(null,"Bajo peso");
            }
            if(indice>18.5 && indice<24.9){
                JOptionPane.showMessageDialog(null,"Peso Normal");
            }
            if(indice>25 && indice<29.9){
                JOptionPane.showMessageDialog(null,"Sobrepeso");
            }
            if(indice>30 && indice<34.9){
                JOptionPane.showMessageDialog(null,"Obesidad I");
            }
            if(indice>35 && indice<39.9){
                JOptionPane.showMessageDialog(null,"Obesidad II");
            }
            if(indice>40 && indice<49.9){
                JOptionPane.showMessageDialog(null,"Obesidad III");
            }
            if(indice>50){
                JOptionPane.showMessageDialog(null,"Obesidad IV");
            }
        }
        else
            JOptionPane.showMessageDialog(null,"Error los datos no son correctos");
        }
}
